/**
 * @file
 * Apply JS library rcSwitcher based on the rcSwitcher module settings.
 */

(function ($) {
  Drupal.behaviors.rcswitcher = {
    attach: function(context, settings) {

      // Verify jQuery version.
      var jquery_version = parseInt(jQuery.fn.jquery.split('.').join(''));
      if (jquery_version < 170) {
        if (typeof console == "object") {
          console.log('rcSwitcher: Error, jQuery version required 1.7.0 >=');
        }
        return;
      }

      var selector = settings.rcswitcher.selector;
      settings.rcswitcher = settings.rcswitcher || Drupal.settings.rcswitcher;

      $('input.rcswitcher-enable, input.rcswitcher-widget', context).once('rcswitcher', function() {
        options = getElementOptions(this);
        $(this).rcSwitcher(settings.rcswitcher.options);
      });

      $(selector, context)
        .filter(function() {
          // Verify element meets widget requirements.
          if (!$(this).attr('name')) {
            return false;
          }
          return true;
        })
        .once('rcswitcher', function(){
          $(this).rcSwitcher(settings.rcswitcher.options);
        });
    }
  };
})(jQuery);
