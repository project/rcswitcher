<?php
/**
 * @file
 * Administration page for rcSwitcher.
 */

/**
 * Builds general configuration form elements.
 */
function rcswitcher_admin_settings($form, &$form_state) {

  $rcswitcher_path = rcswitcher_get_rcswitcher_path();

  if (!$rcswitcher_path) {
    drupal_set_message(t('The library could not be detected. You need to download the !rcSwitcher
      and extract the entire contents of the archive into the %path directory on your server.',
    array(
      '!rcSwitcher' => l(t('rcSwitcher JavaScript file'), RCSWITCHER_WEBSITE_URL),
      '%path' => 'sites/all/libraries',
      )
    ), 'error');
    return;
  }

  $form['rcswitcher_jquery_selector'] = array(
    '#type' => 'textarea',
    '#title' => t('Apply rcSwitcher to the following elements'),
    '#description' => t('A comma-separated list of jQuery selectors to apply rcSwitcher,
      such as <code>.rcswitcher, #edit, input.rcswitcher</code>. '),
    '#default_value' => variable_get('rcswitcher_jquery_selector', 'input[type=checkbox]'),
  );
  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('rcSwitcher options'),
  );
  $form['options']['rcswitcher_use_theme'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use the default rcSwitcher theme'),
    '#default_value' => variable_get('rcswitcher_use_theme', FALSE),
    '#description' => t('Enable or disabled the default rcSwitcher CSS file.'),
  );
  $form['options']['rcswitcher_theme'] = array(
    '#type' => 'textfield',
    '#title' => t('Theme'),
    '#default_value' => variable_get('rcswitcher_theme', 'flat'),
    '#description' => t('Select theme between flat, light, dark, modern'),
  );
  $form['options']['rcswitcher_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
    '#default_value' => variable_get('rcswitcher_width', 80),
    '#description' => t('Width in px'),
    '#element_validate' => array('element_validate_integer_positive'),
  );
  $form['options']['rcswitcher_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Height'),
    '#default_value' => variable_get('rcswitcher_height', 26),
    '#description' => t('Height in px'),
    '#element_validate' => array('element_validate_integer_positive'),
  );
  $form['options']['rcswitcher_blob_offset'] = array(
    '#type' => 'textfield',
    '#title' => t('Blob Offset'),
    '#default_value' => variable_get('rcswitcher_blob_offset', 0),
  );
  $form['options']['rcswitcher_ontext'] = array(
    '#type' => 'textfield',
    '#title' => t('On Text'),
    '#default_value' => variable_get('rcswitcher_ontext', 'YES'),
  );
  $form['options']['rcswitcher_offtext'] = array(
    '#type' => 'textfield',
    '#title' => t('Off Text'),
    '#default_value' => variable_get('rcswitcher_offtext', 'NO'),
  );
  $form['options']['rcswitcher_inputs'] = array(
    '#type' => 'checkbox',
    '#title' => t('Inputs'),
    '#default_value' => variable_get('rcswitcher_inputs', FALSE),
  );
  $form['options']['rcswitcher_autofontsize'] = array(
    '#type' => 'checkbox',
    '#title' => t('Auto Font Size'),
    '#default_value' => variable_get('rcswitcher_autofontsize', TRUE),
  );
  $form['options']['rcswitcher_autostick'] = array(
    '#type' => 'checkbox',
    '#title' => t('Auto Stick'),
    '#default_value' => variable_get('rcswitcher_autostick', TRUE),
  );
  return system_settings_form($form);
}
