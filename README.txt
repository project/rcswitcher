CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------
The rcSwitcher module provides an implementation to attachec the JS library
rcSwitcher JS to checkbox and radio button form elements to make them friendly
changing them to a switch interface.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/andrefy/2482061

REQUIREMENTS
------------
This module requires the following modules:
 * jQuery Update (https://www.drupal.org/project/jquery_update)

This module requires de following libraries:
 * http://ahmad-sa3d.github.io/rcswitcher/

INSTALLATION
------------
 * Download the following library http://ahmad-sa3d.github.io/rcswitcher/

 * Under libraries folder and verify the following file exist:
   libraries/rcswitcher/js/rcswitcher.min.js

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
 * Configure rcSwitcher properties under Administration » Config »
   User Interface >> rcSwitcher:

TROUBLESHOOTING
---------------
 * If the widget does not display, check the following:

   - Verify the jQuery version used by Drupal is 1.7.0 >=
   - Verify a default theme is selected if any custom theme has been included

MAINTAINERS
-----------
Current maintainers:
 * Andres Yajamin (andrefy) - https://www.drupal.org/u/andrefy
